import './App.css';
import { BrowserRouter as Router, Route, Switch, Link } from 'react-router-dom'
import StudentList from "./pages/StudentList"
import AddStudent from "./pages/AddStudent"
import Student from "./pages/Student"
import Login from "./pages/Login"
import Register from "./pages/Register"
import Search from "./pages/Search"
import { AuthContext } from "./helpers/AuthContext"
import { useState, useEffect } from "react"
import axios from 'axios'

function App() {

  const [authState, setAuthState] = useState({
    userName: "",
    id: 0,
    status: false,
  })

  useEffect(() => {
    axios.get('http://localhost:3001/auth/auth', {
       headers: {
        accessToken: localStorage.getItem('accessToken'),
      }
  }).then((response) => {
      if(response.data.error){
        setAuthState({...authState, status: false})
      } else {
        setAuthState({
          userName: response.data.userName,
          id: response.data.id,
          status: true,
        })
      }
    })
  }, [])

  const logOut = () => {
    localStorage.removeItem("accessToken")
    setAuthState({
      userName: "",
      id: 0,
      status: false,
    })
    
  }

  return (
    <div className="App">
      <AuthContext.Provider value={{ authState, setAuthState }}>
      <Router>
        <div className="routeLinkContainer">
        
          
          {!authState.status ? (
            <>
            <Link className="routeLink" to="/login">Prisijungti</Link>
            <Link className="routeLink" to="/register">Registruotis</Link>
            </>
          ) : (
            <>
            <Link className="routeLink" to="/studentlist">Studentų sąrašas</Link>
            <Link className="routeLink" to="/addStudent">Pridėti studentą</Link>
            <Link className="routeLink" to="/search">Paieška</Link>
            <button className="routeLink" onClick={logOut}> Atsijungti </button>
            </>
          )}
          <h1>{authState.userName}</h1>
        </div>
        <Switch>
          <Route path="/" exact component={Login}/>
          <Route path="/studentlist" exact component={StudentList}/>
          <Route path="/addStudent" exact component={AddStudent}/>
          <Route path="/student/:id" exact component={Student}/>
          <Route path="/login" exact component={Login}/>
          <Route path="/register" exact component={Register}/>
          <Route path="/search" exact component={Search}/>

        </Switch>
      </Router>
      </AuthContext.Provider>
    </div>
  ); 
}

export default App;
