import React, { useState, useContext } from 'react'
import axios from 'axios'
import { useHistory } from 'react-router-dom'
import { AuthContext } from '../helpers/AuthContext'

function Login() {

    const [userEmail, setUserEmail] = useState("")
    const [userPassword, setUserPassword] = useState("")
    let history = useHistory()
    const {setAuthState} = useContext(AuthContext)

    const login = () => {
        const data = {userEmail: userEmail, userPassword: userPassword}
        axios.post('http://localhost:3001/auth/login', data).then((response) => {
            if (response.data.error){
                alert(response.data.error)
            } else {
                localStorage.setItem("accessToken", response.data.token)
                setAuthState({userName: response.data.userName, id: response.data.id, status: true})
                history.push("/studentlist")
            }
            
        })
    }

    return (
        <div className="addStudentContainer">
            <h2>Prisijungti</h2>
                <div className="studentInputContainer">
                    <input className="inputAddStudent" name="userEmail" placeholder="Email" onChange={(event) => {
                        setUserEmail(event.target.value)
                    }}/>

                    <input className="inputAddStudent" name="userPassword" type="password" placeholder="slaptažodis" onChange={(event) => {
                        setUserPassword(event.target.value)
                    }} />
                    
                    <button className="fieldButtons" onClick={login}>Prisijungti</button>
                </div>
        </div>
    )
}

export default Login
