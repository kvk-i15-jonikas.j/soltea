import React from 'react'
import { Formik, Form, Field, ErrorMessage } from 'formik'
import * as Yup from 'yup'
import axios from 'axios'

function Register() {
    const initialValues = {
        userName: "",
        userPassword: "",
        userEmail: "",
    }

    const validationSchema = Yup.object().shape({
        userName: Yup.string().min(4).max(20).required("Klaida"),
        userPassword: Yup.string().min(4).max(20).required("Klaida"),
        userEmail: Yup.string().email('Invalid email').required('Required'),
    })

    const onSubmit = (data) => {
        axios.post("http://localhost:3001/auth", data).then(() => {
            console.log(data)
        })
    }

    return (
        <div className="addStudentContainer">
            <h2>Registruotis</h2>
            <Formik initialValues={initialValues} onSubmit={onSubmit} validationSchema={validationSchema}>
                
                <Form className="studentInputContainer">

                    <ErrorMessage name="userName" component="span"/>
                    <Field className="inputAddStudent" name="userName" placeholder="Vardas" />

                    <ErrorMessage name="userEmail" component="span"/>
                    <Field className="inputAddStudent" name="userEmail" placeholder="Email" />

                    <ErrorMessage name="userPassword" component="span"/>
                    <Field className="inputAddStudent" name="userPassword" type="password" placeholder="slaptažodis" />
                    
                    <button className="fieldButtons" type="submit">Registruotis</button>
                </Form>
            </Formik>
        </div>
    )
}

export default Register
