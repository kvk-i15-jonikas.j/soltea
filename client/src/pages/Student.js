import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import axios from 'axios'
import { Formik, Form, Field, ErrorMessage } from 'formik'
import * as Yup from 'yup'

function Student() {
    let {id} = useParams()
    const [studentObj, setStudentObj] = useState({})

    useEffect(() => {
        axios.get(`http://localhost:3001/student/byid/${id}`).then((response) => {
            setStudentObj(response.data)
        })
    }, [])

    const initialValues = {
        nameFirst: studentObj.nameFirst,
        nameLast: studentObj.nameLast,
        studentClass: studentObj.studentClass,
        entryDate: studentObj.entryDate,
        entryMessage: studentObj.entryMessage,
    }

    const validationSchema = Yup.object().shape({
        nameFirst: Yup.string().max(20),
        nameLast: Yup.string().max(20),
        studentClass: Yup.string().max(3),
        entryDate: Yup.date(),
        entryMessage: Yup.string().max(150),
    })

    const onSubmit = (data) => {
        axios.put(`http://localhost:3001/student/changebyid/${id}`, data).then((response) => {})
    }

    return (
        <div className="addStudentContainer">
        <h2>Įveskite pakeitimus</h2>
        <Formik initialValues={initialValues} onSubmit={onSubmit} validationSchema={validationSchema}>
            
            <Form className="studentInputContainer">
                <ErrorMessage name="nameFirst" component="span"/>
                <Field className="inputAddStudent" name="nameFirst" placeholder={studentObj.nameFirst} />

                <ErrorMessage name="nameLast" component="span"/>
                <Field className="inputAddStudent" name="nameLast" placeholder={studentObj.nameLast} />

                <ErrorMessage name="studentClass" component="span"/>
                <Field className="inputAddStudent" name="studentClass" placeholder={studentObj.studentClass} />

                <ErrorMessage name="entryDate" component="span"/>
                <Field className="inputAddStudent" type="date" name="entryDate" placeholder={studentObj.entryDate} />

                <ErrorMessage name="entryMessage" component="span"/>
                <Field className="inputAddStudent inputTextArea" component="textarea" name="entryMessage" placeholder={studentObj.entryMessage}/>
                
                <button className="fieldButtons" type="submit">Atlikti Keitimus</button>
            </Form>
        </Formik>
    </div>
    )
}

export default Student
