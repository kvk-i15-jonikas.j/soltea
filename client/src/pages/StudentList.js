import React from 'react'
import ListOfStudents from '../components/StudentsComponents/ListOfAllStudents'
function StudentList() {
    return (
        <div className="studentListContainer">
            <ListOfStudents />
        </div>

    )
}

export default StudentList
