import React, { useState } from 'react'
import { Formik, Form, Field, ErrorMessage } from 'formik'
import * as Yup from 'yup'
import axios from 'axios'
import "./search.css"

function Search() {

    const [isChecked, setIsChecked] = useState(false)
    const [listOfStudents, setListOfStudents] = useState([])

    const initialValues = {
        nameFirst: "",
        nameLast: "",
        studentClass: "",
        entryDate: "",
    }

    const validationSchema = Yup.object().shape({
        nameFirst: Yup.string().max(20),
        nameLast: Yup.string().max(20),
        studentClass: Yup.string().max(3),
        entryDate: Yup.date(),
    })

    const onSubmit = (data) => {
        if(isChecked){
            axios.post("http://localhost:3001/student/searchhard", data, {headers: {accessToken: localStorage.getItem("accessToken")}}).then((response) => {
                setListOfStudents(response.data)
            })
        } else{
            axios.post("http://localhost:3001/student/searchsoft", data, {headers: {accessToken: localStorage.getItem("accessToken")}}).then((response) => {
                setListOfStudents(response.data)
            })
        }
    }

    return (
        <div className="searchContainer">
        <Formik initialValues={initialValues} onSubmit={onSubmit} validationSchema={validationSchema}>
            
            <Form className="searchInputContainer">
                <div role="group" aria-labelledby="checkbox-group">
                    <label>
                    Tiksli paieška: 
                    <Field type="checkbox" name="checked" value="tiksli" checked={isChecked} onChange={() => {
                         if (isChecked){
                             setIsChecked(false)
                         } else {
                             setIsChecked(true)
                         }
                    }} />
                    </label>
                </div>
                <ErrorMessage name="nameFirst" component="span"/>
                <Field className="searchInputItem" name="nameFirst" placeholder="Vardas" />

                <ErrorMessage name="nameLast" component="span"/>
                <Field className="searchInputItem" name="nameLast" placeholder="Pavardė" />

                <ErrorMessage name="studentClass" component="span"/>
                <Field className="searchInputItem" name="studentClass" placeholder="Klasė (2a, 11b...)" />

                <ErrorMessage name="entryDate" component="span"/>
                <Field className="searchInputItem" type="date" name="entryDate" />
                
                <button className="searchButton" type="submit">Ieškoti</button>
            </Form>
        </Formik>
        <div className="resultContainer">
        {
            listOfStudents.map((value, key) => {
                return (
                    <ul className="studentInfoContainer">
                        <li className="studentInfoItem">{value.id}</li>
                        <li className="studentInfoItem">{value.nameFirst}</li>
                        <li className="studentInfoItem">{value.nameLast}</li>
                        <li className="studentInfoItem">{value.studentClass}</li>
                        <li className="studentInfoItem">{value.entryDate}</li>
                        <li className="studentInfoItem">{value.entryMessage}</li>
                    </ul>
                )
            })
        }
        </div>
    </div>
    )
}

export default Search
