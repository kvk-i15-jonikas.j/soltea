import React, { useState } from 'react'
import { Formik, Form, Field, ErrorMessage } from 'formik'
import './addStudent.css'
import * as Yup from 'yup'
import axios from 'axios'


function AddStudent() {

    const initialValues = {
        nameFirst: "",
        nameLast: "",
        studentClass: "",
        entryDate: "",
        entryMessage: "",
    }

    const validationSchema = Yup.object().shape({
        nameFirst: Yup.string().min(4).max(20).required("Klaida"),
        nameLast: Yup.string().min(4).max(20).required("Klaida"),
        studentClass: Yup.string().min(2).max(3).required("Klaida"),
        entryDate: Yup.date().required("Klaida"),
        entryMessage: Yup.string().min(4).max(150).required("Klaida"),
    })

    const onSubmit = (data) => {
        axios.post("http://localhost:3001/student", data, {headers: {accessToken: localStorage.getItem("accessToken")}}).then((response) => {
            alert("Studentas Pridėtas")
        })
    }

    
    

    return (
        <div className="addStudentContainer">
            <h2>Užpildykite formą naujo studento įrašo pridėjimui</h2>
            <Formik initialValues={initialValues} onSubmit={onSubmit} validationSchema={validationSchema}>
                
                <Form className="studentInputContainer">
                    <ErrorMessage name="nameFirst" component="span"/>
                    <Field className="inputAddStudent" name="nameFirst" placeholder="Vardas" />

                    <ErrorMessage name="nameLast" component="span"/>
                    <Field className="inputAddStudent" name="nameLast" placeholder="Pavardė" />

                    <ErrorMessage name="studentClass" component="span"/>
                    <Field className="inputAddStudent" name="studentClass" placeholder="Klasė (2a, 11b...)" />

                    <ErrorMessage name="entryDate" component="span"/>
                    <Field className="inputAddStudent" type="date" name="entryDate" placeholder="Data" />

                    <ErrorMessage name="entryMessage" component="span"/>
                    <Field className="inputAddStudent inputTextArea" component="textarea" name="entryMessage" placeholder="..." />
                    
                    <button className="fieldButtons" type="submit">Pridėti Studentą</button>
                </Form>
            </Formik>
        </div>
    )
}

export default AddStudent
