import React from 'react';
import { useEffect, useState } from 'react'
import axios from 'axios'
import '../StudentsComponents/students.css'
import { useHistory } from 'react-router-dom'


const ListOfAllStudents = () => {
    
  const [listOfStudents, setListOfStudents] = useState([])
  let history = useHistory()



  useEffect(() => {
    axios.get("http://localhost:3001/student").then((response) => {
    setListOfStudents(response.data)
    })
  }, []);

    return (
        <div>
            {listOfStudents.map((value, key) => {
                return (
                    <ul className="studentInfoContainer" onClick={() => {history.push(`/student/${value.id}`)}}>
                        <li className="studentInfoItem">{value.id}</li>
                        <li className="studentInfoItem">{value.nameFirst}</li>
                        <li className="studentInfoItem">{value.nameLast}</li>
                        <li className="studentInfoItem">{value.studentClass}</li>
                        <li className="studentInfoItem">{value.entryDate}</li>
                        <li className="studentInfoItem">{value.entryMessage}</li>
                    </ul>
                )
            })}
        </div>

    );
};

export default ListOfAllStudents;