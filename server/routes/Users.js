const express = require('express');
const router = express.Router();
const { Users } = require('../models');
const bcrypt = require('bcrypt');
const { sign } = require('jsonwebtoken')
const { validateToken } = require('../middlewares/AuthMiddleware')

router.post("/", async (req, res) => {
    const { userName, userEmail, userPassword } = req.body
    bcrypt.hash(userPassword, 10).then((hash) => {
        Users.create({
            userName: userName,
            userPassword: hash,
            userEmail: userEmail,
        })
        res.json("Registering .>?")
    })

});

router.post("/login", async (req, res) => {
    const { userEmail, userName, userPassword } = req.body
    const user = await Users.findOne({where: {userEmail: userEmail}})

    if (!user) return res.json({ error: "Neteisingas prisijungimo paštas"})

    bcrypt.compare(userPassword, user.userPassword).then((match) => {
        if(!match) return res.json({ error: "Neteisingas slaptažodis"})
        const accessToken = sign({userName: user.userEmail, id: user.id}, "logintokensecret") 
        res.json({token: accessToken, userName: userName, id: user.id})
    })

});

router.get("/auth", validateToken, (req, res) => {
    res.json(req.user)
})


module.exports = router;