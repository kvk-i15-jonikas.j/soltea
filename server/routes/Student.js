const express = require('express');
const router = express.Router();
const { Students, Sequelize } = require('../models')
const { validateToken } = require('../middlewares/AuthMiddleware');

router.get("/", async (req, res) => {
    const listOfStudents = await Students.findAll()
    res.json(listOfStudents)
});

router.get("/byid/:id", async (req, res) => {
    const id = req.params.id
    const student = await Students.findByPk(id)
    res.json(student)
})

router.post("/", validateToken, async (req, res) => {
    const student = req.body;
    await Students.create(student);
    res.json(student);
});
router.put('/changebyid/:id', validateToken, async (req, res) => {
    const id = req.params.id
    const student = req.body
    await Students.update({
            nameFirst: student.nameFirst, 
            nameLast: student.nameLast, 
            studentClass: student.studentClass, 
            entryDate: student.entryDate, 
            entryMessage: student.entryMessage }, 
        { where: {id: id}})
    res.json(student.entryMessage)
    
})

router.post('/searchhard', validateToken, async (req, res) => {
    const student = req.body
    const studentList = await Students.findAll({
        where: {
            nameFirst: student.nameFirst,
            nameLast: student.nameLast,
            studentClass: student.studentClass,
            entryDate: student.entryDate
        }
    })
    res.send(studentList)
})

router.post('/searchsoft', validateToken, async (req, res) => {
    const student = req.body
    const studentList = await Students.findAll({ where: Sequelize.or(
        {nameFirst: [student.nameFirst, ""]},
        {nameLast: [student.nameLast, ""]},
        {studentClass: [student.studentClass, ""]},
        {entryDate: [student.entryDate, ""]}
    )})
    res.send(studentList)
})

module.exports = router;