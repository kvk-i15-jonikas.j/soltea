const { verify } = require('jsonwebtoken')


const validateToken = (req, res, next) => {

    const accessToken = req.header("accessToken")

    if (!accessToken) return res.json({error: "Neprisijungęs vartotojas"})

    try {
        const validToken = verify(accessToken, "logintokensecret")
        if(validToken) return next()
    } catch (err) {
        return res.json({error: err})
    }
    
}
module.exports = { validateToken }