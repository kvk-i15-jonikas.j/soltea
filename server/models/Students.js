const { DataTypes } = require('sequelize');

module.exports = (sequelize) =>  {
    const Students = sequelize.define("Students", {
        id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        nameFirst: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        nameLast: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        studentClass: {
            type: DataTypes.STRING(3),
            allowNull: false,
        },
        entryDate: {
            type: DataTypes.DATEONLY,
            allowNull: false,
        },
        entryMessage: {
            type: DataTypes.TEXT,
            allowNull: false,
        },
    })

    return Students
}